
#ifndef LINKTHREAD_H
#define LINKTHREAD_H

void un_link();
void init();
void display_flush (void);
int link_thread();
void release_network();
#define FILTER_STR "ether[12:2]=0x888e and ether dst %02x:%02x:%02x:%02x:%02x:%02x"

#endif
