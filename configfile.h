#ifndef _CONFIGFILE_H_
#define _CONFIGFILE_H_

typedef struct _Config_Info
{
	int     auto_retry;
	int	response_time_out,
		retry_times,
		refresh_time_out;
	char	*interface;
}Config_Info;

int load_config(void);

void free_config(void);

#endif
