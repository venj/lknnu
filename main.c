#include "global.h"
#include "linkthread.h"
#include "configfile.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>

int
main (int argc, char *argv[])
{
	//no argument needed, so delete argc section.
	//暂时去掉断开连接所使用的命令行参数定义。
	signal(SIGINT, un_link); //这个应该是用于断开已经运行中的客户端程序。
	
	if (getuid() != 0)
	{
		fputs(">> Only root users can run this procedure\n", stdout); 
		return EXIT_FAILURE;
	}

	
	init();
	link_thread();
	return EXIT_SUCCESS;
}

