VERSION = 2.1.0
BIN_DIR = /usr/local/bin
CONFIG_DIR = /etc
CONFIG_FILE = lknnu.conf

MAKE := gmake
MAKEFLAGS := -j1
CXX := gcc
CFLAGS :=  -O2 -Wall
LD := $(CXX)
LDOPTION := -lnet -lpcap

TARGETS :=lknnu
OBJECTS := main.o  configfile.o linkthread.o  sendpacket.o myerr.o

all: $(TARGETS)

lknnu : $(OBJECTS)
	$(LD) -o $@ $^ $(LDOPTION)

install : lknnu
	install $^ $(BIN_DIR)
	install $(CONFIG_FILE) $(CONFIG_DIR)
uninstall: 
	rm -f $(BIN_DIR)/$(TARGETS)
	rm -f $(CONFIG_DIR)/$(CONFIG_FILE)
clean:
	rm *.o lknnu
